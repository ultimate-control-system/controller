#include <iostream>

#include "DdsSubscriber.hpp"
#include "DdsPublisher.hpp"

#include "TorqueControl/TorqueControlPubSubTypes.h"
#include "CartPoleFeedback/CartPoleFeedbackPubSubTypes.h"
#include <csignal>

volatile sig_atomic_t Stop;
using namespace eprosima::fastdds::dds;

DomainParticipant *Participant;

DdsPublisher<TorqueControlPubSubType> *TorquePublisher;
DdsSubscriber<CartPoleFeedbackPubSubType> *CartPoleSubscriber;
DdsSubscriber<CartPoleFeedbackPubSubType> *SetpointSubscriber;

double SwingUp(double angle, double angleDot)
{
    double g = 9.81;
    double massPole = 10;// kg
    double lengthPole = 0.6;// m
    double inertiaPole = 0.05;// m^2

    double eP = 0.5 * inertiaPole * pow(angleDot, 2) + g * lengthPole * massPole * (cos(angle) - 1);

    return -eP * angleDot * cos(angle) * 0.4525;
}

bool Flag = true;
double CartSetpoint = 0.0;

int main()
{

    std::cout << "Starting Controller." << std::endl;
    signal(SIGTERM, [](int signum)
    { Stop = 1; });
    // Create node
    DomainParticipantQos participantQos;
    participantQos.name("State feedback controller");
    Participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (Participant == nullptr)
    {
        return 1;
    }
    TorquePublisher = new DdsPublisher<TorqueControlPubSubType>(Participant, "TorqueControl");

    CartPoleSubscriber = new DdsSubscriber<CartPoleFeedbackPubSubType>(Participant, "CartPoleFeedback");

    CartPoleSubscriber->bindDataAvailableCallback(
        [=](const CartPoleFeedback &msg, const SampleInfo &info) {
            // RUN STATE FEEDBACK CONTROLLER
            TorqueControl control;

            //std::cout << "X: " << msg.cart_x() << " T: " << msg.pole_theta() << std::endl;
            double k[4] = {-31.6227766, -51.12423315, 584.51134517, 110.81444504};

            double theta = msg.pole_theta();
            theta -= floor((theta / (M_PI * 2)) + 0.5) * (M_PI * 2);

            double f = SwingUp(theta, msg.pole_theta_dot());
            //std::cout<<theta<<std::endl;
            if (theta < 0.15 && theta > -0.15)
              Flag = false;
            if (theta > M_PI_2 || theta < -M_PI_2)
              Flag = true;

            if (Flag)
            {
              control.torque_nm() = f / 5;
              //Flag = false;
            } else
            {
              control.torque_nm() = -(
                      (msg.cart_x() - CartSetpoint) * k[0] +
                      msg.cart_x_dot() * k[1] +
                      theta * k[2] +
                      msg.pole_theta_dot() * k[3]
              );
            }
            TorquePublisher->publish(control);
        }
    );

    SetpointSubscriber = new DdsSubscriber<CartPoleFeedbackPubSubType>(Participant, "CartPoleSetpoint");
    SetpointSubscriber->bindDataAvailableCallback(
        [=](const CartPoleFeedback &msg, const SampleInfo &info) {
            std::cout << "Received new setpoint: " << msg.cart_x() << std::endl;
            CartSetpoint = msg.cart_x();
        }
    );

    while (!Stop)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    return 0;
}
