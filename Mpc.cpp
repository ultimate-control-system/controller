#include <iostream>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include "DdsSubscriber.hpp"
#include "DdsPublisher.hpp"

#include "TorqueControl/TorqueControlPubSubTypes.h"
#include "CartPoleFeedback/CartPoleFeedbackPubSubTypes.h"
#include <signal.h>

volatile sig_atomic_t Stop;
using namespace eprosima::fastdds::dds;
using namespace boost::numeric::ublas;

DomainParticipant *Participant;
DdsPublisher<TorqueControlPubSubType> *TorquePublisher;
DdsSubscriber<CartPoleFeedbackPubSubType> *CartPoleSubscriber;
//According to the boost matrix documentation, there are 3 constructors for the matrix class: empty, copy, and one taking two size_types for the number of rows and columns. Since boost doesn't define it (probably because there are many ways to do it and not every class is gong to define a conversion into every other class) you are going to need to define the conversion.

//Here's an approach that I would use, but since there are multiple ways to do this and the question doesn't specify how you want this done you may find a different approach more applicable to your situation.

void Foo(const std::vector<double> & v) {
    size_t m = 4; // you need to specify
    size_t n = 4; // you need to specify

    if(v.size() < m * n)   { // the vector size has to be bigger or equal than m * n
        // handle this situation
    }

    matrix<double> mat(m, n);
    for(size_t i=0; i<mat.size1(); i++) {
        for(size_t j=0; j<mat.size2(); j++) {
            mat(i,j) = v[i+j*mat.size1()];
        }
    }
}

int main() {
    std::cout << "Starting Controller." << std::endl;
    signal(SIGTERM, [](int signum) { Stop = 1; });
    // Create node
    DomainParticipantQos participantQos;
    participantQos.name("State feedback controller");
    Participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (Participant == nullptr) {
        return 1;
    }
    TorquePublisher = new DdsPublisher<TorqueControlPubSubType>(Participant, "TorqueControl");

    CartPoleSubscriber = new DdsSubscriber<CartPoleFeedbackPubSubType>(Participant, "CartPoleFeedback");

    CartPoleSubscriber->bindDataAvailableCallback([=](const CartPoleFeedback &msg, const SampleInfo &info) {
        // RUN STATE FEEDBACK CONTROLLER
        TorqueControl control;
        std::cout << "X: " << msg.cart_x() << " T: " << msg.pole_theta() << std::endl;
        // Constants
        double m=1;
        double l=1;
        double M=1;
        double g=9.8;

        //double A=[0 0 1 0;0 0 0 1;(m+M)*g/l/m 0 0 0;-m*g/M 0 0 0];
        double B[4]={0, 0, -1/l/M, 1/M};

        double K[4] = {-31.6227766, -51.12423315, -584.51134517, -110.81444504};
        control.torque_nm() = -(
                msg.cart_x() * K[0] +
                msg.cart_x_dot() * K[1] +
                msg.pole_theta() * K[2] +
                msg.pole_theta_dot() * K[3]
        );
        TorquePublisher->publish(control);
    });


    while (!Stop) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    return 0;
}
