//
// Created by Magnus Kjelland on 06/10/2022.
//

#include <Eigen/Eigen>
#include <iostream>
#include <Eigen/Dense>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::Matrix;

int main()
{
    Matrix<double, 2, 2> b {
            {2, 4},
            {5, 6},
    };
    Matrix<double, 2, 3> c {
            {2, 4,2},
            {2, 4,2},

    };
    auto d = b*c;

    std::cout<< b*c<<std::endl;

    MatrixXd m = MatrixXd::Random(3,3);
    m<<1,2,3,1,2,3,1,2,3;
    std::cout << "m =" << std::endl << m << std::endl;
    VectorXd v(3);
    v << 1, 2, 3;
    std::cout << "v =" << std::endl << v << std::endl;

    std::cout << "m * v =" << std::endl << m * m << std::endl;
}